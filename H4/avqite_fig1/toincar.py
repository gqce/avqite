import json, numpy


def partial2full(s, nq):
    ans = ['I']*nq
    if s != 'I':
        for i in range(1, len(s), 2):
            ans[int(s[i])] = s[i-1]
    return ''.join(ans)


ops = numpy.loadtxt("Puccsd_pool_nq6_np4.dat", dtype=str).tolist()
ops = [op[::-1] for op in ops]
nq = len(ops[0])
hs = numpy.loadtxt("h4trial_r2.4.dat", dtype=str).tolist()
for i, h in enumerate(hs):
    h = h.split('*')
    hs[i] = h[0]+'*'+partial2full(h[1], nq)

data = {}
data['pool'] = ops
data['h'] = hs
data['ref_state'] = '001001'

with open("incar", "w") as f:
    json.dump(data, f, indent=4)
