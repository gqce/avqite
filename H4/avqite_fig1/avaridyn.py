import h5py, numpy
from qutip import Qobj
import pdb
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from matplotlib import rc
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import copy
from scipy import linalg as SciLA


def best_fit(data):
    n = len(data)
    print(n)
    data_ = copy.deepcopy(data)
    #data = data[int(9*n/10):,:]
    #data_ = data[-50:,:]

    print(data_.shape)
    #x = data_[:,0]
    x = numpy.log(data_[-5:,0])
    y = data_[-5:,1]
    #y = numpy.log(data_[:,1])

    xm = numpy.mean(x)
    ym = numpy.mean(y)

    xy = numpy.dot(x-xm,y-ym)
    xx= numpy.dot(x-xm,x-xm)
    slope = xy/xx
    y0 = ym - slope*xm
    return slope, y0


def log_fit(data):
    n = len(data)
    print(n)
    data_ = copy.deepcopy(data)
    #data = data[int(9*n/10):,:]
    #data_ = data[-50:,:]

    print(data_.shape)
    x = data_[:,0]
    y = data_[:,1]
    #y = numpy.log(data_[:,1])

    #y = A + B log x
    ans = numpy.polyfit(numpy.log(x), y, 1)
    A = ans[0]
    B = ans[1]
    return A,B


def poly_fit(data):
    n = len(data)
    print(n)
    data_ = copy.deepcopy(data)
    #data = data[int(9*n/10):,:]
    #data_ = data[-50:,:]

    print(data_.shape)
    x = data_[-5:,1]
    y = data_[-5:,2]
    #y = numpy.log(data_[:,1])

    #y = A + B log x
    ans = numpy.polyfit(x, y, 1)
    A = ans[0]
    B = ans[1]
    return A,B

class avaridynBase:
    # adaptive variational quantum dynamics sumulation.
    def __init__(self,
            model,    # the model
            ansatz,   # the ansatz
            dtmax=0.05,  # maximal time step size
            dthmax=0.02,  # maximal theta step size
            ):
        self._model = model
        self._ansatz = ansatz
        self._dtmax = dtmax
        self._dthmax = dthmax #
        self._dt = dtmax

    def run(self, path="/0/", mode=0):
        # initial state
        e_exact = self.set_initial_state(mode=mode)
        #ansatz adaptive optimization
        pool_order = self._ansatz._order
        r_cut = self._ansatz._rcut
        mmdfile = open("mmd_record.dat",'w')
        ansatzfile = "ansatz_record.dat"
        #pool_order = 4
        out_filname = self._model._filename[:-4]+'N'+str(pool_order)+'rcut'+str(r_cut)+'_dt'+str(self._dt)+str('.out')
        #pdb.set_trace()
        fout = open(out_filname,'w')
        energy_filename = 'Energy'+self._model._filename[:-4]+'N'+str(pool_order)+'rcut'+str(r_cut)+'_dt'+str(self._dt)+str('.out')

        qlanc_filename = 'EQlanc'+self._model._filename[:-4]+'N'+str(pool_order)+'rcut'+str(r_cut)+'_dt'+str(self._dt)+str('.out')
        if self._model._evolution == 'R':
            self._ansatz.additive_optimize(self._model.get_h(0))
        print(f"Size of the operator pool: {len(self._ansatz._op_pools[0][0])}",file = fout)
        print(f"Size of the operator pool: {len(self._ansatz._op_pools[0][0])}")
        print(f"Exact energy 0: {e_exact[0]:.10f}",file = fout)
        print(f"Exact energy 0: {e_exact[0]:.10f}")
        self._ansatz.construct_psi0()
        vec = self._ansatz.get_state()
        e_ansatz = self._model.get_h_expval(0, vec)
        print(f"ansatz energy: {e_ansatz:.10f}",file = fout)
        print(f"ansatz energy: {e_ansatz:.10f}")
        if self._model._evolution == 'R':
            if self._ansatz._opdense:
                ovlp = abs(numpy.vdot(vec, self._vec_i))
            else:
                ovlp = abs(vec.overlap(self._vec_i))
            print(f"initial state overlap: {ovlp:.10f}")
        else:
            w, v = self._model._h.eigenstates(eigvals=3)
            if self._ansatz._opdense:
                ovlp = abs(numpy.vdot(vec, v[0]))
            else:
                ovlp = abs(vec.overlap(v[0]))
            print(f"initial-exact state overlap: {ovlp:.10f}")

        self._t_list = []
        energy_data = []
        qlanc_data = []
        ansatz_data = []
        ic = 0
        eq_data = []
        eq1_data = []
        norm0 = 1
        old_variance = 1000
        with open("params_trace.dat", "w") as f:
            t = 0.
            #while t < self._model._T + 0.01:
            while True:
                h = self._model.get_h(t)

                dt, energy, variance, norm = self._ansatz.one_step(h,
                        self._dtmax,
                        self._dthmax,
                        self._dt,
                        mmdfile
                        )
                norm = 1/numpy.sqrt(1-2*dt*energy)
                #norm = 1/numpy.sqrt(1-2*dt*energy+dt*dt*variance)
                num_op = len(self._ansatz._ansatz[0])
                ngates = self._ansatz.ngates
                print(f"ngates: {ngates}")
                print(f"ngates: {ngates}",file=fout)
                print(f'dt: {dt:.5f}')
                print(f"variance: {variance:.10f}, energy: {energy:.10f}",file = fout)
                print(f"variance: {variance:.10f}, energy: {energy:.10f}")
                fout.flush()
                energy_data.append([ t, variance, energy])
                ansatz_data.append([ t, num_op])
                self._t_list.append(t)
                #self.update_records(t)
                qlanc_data.append([t,energy,norm])
                if ic%2==1:
                    eq = qlanc(ic,qlanc_data,0.95,dlanz=1e-14)
                    eq_data.append([t,eq[0]])
                    print(f"Qlanc energy: {eq[0]:.10f}")
                    print(f"Qlanc energy: {eq[0]:.10f}",file = fout)
                f.write(f"{t:.6f}  " + "  ".join(f"{th:.12f}" \
                        for th in self._ansatz._params) + "\n")
                t += dt

                ic += 1
                if abs(energy - e_exact[0]) < 1.e-4:
                    print(f"Chemical accuracy reachd in in {ic} steps",file=fout)
                    print(f"Chemical accuracy reachd in in {ic} steps")
                    break
                if ic > 1:
                    if abs(e_ansatz-energy) < 1.e-10:
                        print(f"Stopped due to convergence in {ic} steps",file=fout)
                        print(f"Stopped due to convergence in {ic} steps")
                        break
                    e_ansatz = energy
                if old_variance < variance:
                    print("Stopped due to increase in variance", file=fout)
                    print("Stopped due to increase in variance")
                    #break
                old_variance = variance


        vec = self._ansatz.get_state()
        e_ansatz = self._model.get_h_expval(0, vec)
        print(f"final ansatz energy: {e_ansatz:.10f}")
        if self._model._evolution != 'R':
            w, v = self._model._h.eigenstates(eigvals=3)
            if self._ansatz._opdense:
                ovlp = abs(numpy.vdot(vec, v[0]))
            else:
                ovlp = abs(vec.overlap(v[0]))
            print(f"final-exact state overlap: {ovlp:.10f}")

        num_op = len(self._ansatz._ansatz[0])
        self._ansatz.show()
        print("Number of operators in the final ansatz: "+str(num_op),file = fout)
        print(f"Compare with initial ansatz energy: {e_ansatz:.10f}",file = fout)
        print(f"Compare with the exact energy 0: {e_exact[0]:.10f}",file = fout)
        print(f"Compare with the exact energy 1: {e_exact[1]:.10f}",file = fout)
        print(f"Compare with the exact energy 2: {e_exact[2]:.10f}",file = fout)
        print(f"pthmaxt = {self._ansatz._pthmaxt:.2e}", file=fout)

        print(f"qLanczos E0: {eq[0]:.10f}",file = fout)
        print(f"qLanczos E1: {eq[1]:.10f}",file = fout)
        energy_data = numpy.asarray(energy_data)
        eq_data = numpy.asarray(eq_data)
        ansatz_data = numpy.asarray(ansatz_data)
        numpy.savetxt(energy_filename,energy_data)
        numpy.savetxt(qlanc_filename,eq_data)
        numpy.savetxt(ansatzfile,ansatz_data)

        A, B = poly_fit(energy_data)
        print(f"Slope = {A:.2f} ; Intercept = {B:.2f}",file = fout)
        print(f"Slope = {A:.2f} ; Intercept = {B:.2f}")
        #print(f"Extrapolated energy: {y0:.10f}")
        print(f"Extrapolated energy: {B:.10f}",file = fout)
        print(f"Extrapolated energy: {B:.10f}")
        fout.flush()
        fout.close()

        x1 = 0.1*max(energy_data[:,0])
        x0 = min(numpy.log(energy_data[:,0]))
        x1 = 0.1*max(numpy.log(energy_data[:,0]))


        x0 = min(energy_data[:,0])
        x1 = max(energy_data[:,0])
        x_e = numpy.linspace(0,x1)
        #y_e = A*numpy.log(x_e) + B
        y_e = A*(x_e) + B
        plt.plot(energy_data[:,0],energy_data[:,2], '.', label = "QITE" )
        plt.plot(eq_data[0:,0],eq_data[0:,1], '*--',label = "qLanc" )
        plt.xlabel(r"$\tau$")
        plt.ylabel(r"$E$")
        plt.xlim(0,x1)
        plt.plot([0,x1], [e_exact[0], e_exact[0]], 'r--', label = 'Exact 0')
        plt.legend()
        plt.show(block = False)
        try:
            input("Hit Enter to close")
        except:
            plt.close()


    def set_initial_state(self, mode):
        raise NotImplementedError("set_initial_state not implemented.")

    def init_records(self):
        self._records = {}

    def update_records(self):
        raise NotImplementedError("update_records not implemented.")

    def save_records(self, path):
        with h5py.File("results_asim.h5", "a") as f:
            if path in f:
                del f[path]
            f[f"{path}/t"] = self._t_list
            for key, val in self._records.items():
                f[f"{path}/{key}"] = val


class avaridynXYZ(avaridynBase):
    def init_records(self):
        self._records = {
                "e": [],
                "state": [],
                "ov": [],
                "dist": [],
                "spincorr": [],
                "ngates": [],
                }
    def set_initial_state(self, mode=1):
        # initial state
        #w, v = self._model.get_loweste_states(0)
        w = self._model.get_loweste_states(0)
        # choose the ground state
        print(f"lowest energy states: {' '.join(f'{w1:.5f}' for w1 in w[:2])}")
        #self._vec_i = v[0]
        #if mode == 1:

        #self._ansatz.set_ref_state(self._vec_i)
        # final ground state
        #w, v = self._model.get_loweste_states(self._model._T)
        #print(f"final states energy: {' '.join(f'{w1:.5f}' for w1 in w[:2])}")
        #self._vec_f = v[0]

    def update_records(self, t):
        vec = self._ansatz.state
        e = self._model.get_h_expval(t, vec)
        self._records["e"].append(e)
        if self._ansatz._opdense:
            ovi = abs(numpy.vdot(vec, self._vec_i))
            ovf = abs(numpy.vdot(vec, self._vec_f))
        else:
            ovi = abs(vec.overlap(self._vec_i))
            ovf = abs(vec.overlap(self._vec_f))
        print(f"t = {t:.5f} e = {e:.6f} ovs = {ovi:.6f} {ovf:.6f}")
        self._records["ov"].append([ovi, ovf])
        if self._ansatz._opdense:
            vec = Qobj(vec)
        corrs = [op.matrix_element(vec, vec).real  \
                for op in self._model.corr_ops]
        if isinstance(vec, Qobj):
           vec = vec.full().reshape(-1)
        self._records["state"].append(vec)
        self._records["spincorr"].append(corrs)
        dist = self._ansatz.get_dist()
        self._records["dist"].append(dist)
        ngates = self._ansatz.ngates
        self._records["ngates"].append(ngates)
        print(f"ngates: {ngates}")


class avaridynIsing(avaridynBase):
    def init_records(self):
        self._records = {
                "e": [],
                "state": [],
                "ov": [],
                "dist": [],
                "ngates": [],
                }

    def set_initial_state(self, mode=1):
        # initial state
        w, v = self._model.get_loweste_states(0)
        print(f"lowest energy states: {' '.join(f'{w1:.5f}' for w1 in w[:2])}")
        self._vec_i = numpy.zeros(v[0].shape[0])
        self._vec_i[0] = 1.

    def update_records(self, t):
        vec = self._ansatz.state
        e = self._model.get_h_expval(t, vec)
        self._records["e"].append(e)
        self._records["state"].append(vec.full().reshape(-1))
        # 2-fold degenerate ground state
        ov = abs(numpy.asarray([vec[0], vec[-1]]))
        print(f"t = {t:.5f} e = {e:.6f} ov = {ov[0]:.6f} {ov[1]:.6f}\n")
        self._records["ov"].append(ov)
        dist = self._ansatz.get_dist()
        self._records["dist"].append(dist)
        self._records["ngates"].append(self._ansatz.ngates)



class avaridynMolecule(avaridynBase):
    def init_records(self):
        self._records = {
                "e": [],
                "state": [],
                "ov": [],
                "dist": [],
                "spincorr": [],
                "ngates": [],
                }
    def set_initial_state(self, mode=1):
        # initial state
        w = self._model.get_loweste_states(0)
        # choose the ground state
        print(f"lowest energy states: {' '.join(f'{w1:.5f}' for w1 in w[:2])}")
        return w

    def update_records(self, t):
        vec = self._ansatz.state
        e = self._model.get_h_expval(t, vec)
        print("Energy:"+str(e))
        self._records["e"].append(e)
        if self._ansatz._opdense:
            vec = Qobj(vec)
        #corrs = [op.matrix_element(vec, vec).real  \
        #        for op in self._model.corr_ops]
        if isinstance(vec, Qobj):
           vec = vec.full().reshape(-1)
        self._records["state"].append(vec)
        #self._records["spincorr"].append(corrs)
        dist = self._ansatz.get_dist()
        self._records["dist"].append(dist)
        ngates = self._ansatz.ngates
        self._records["ngates"].append(ngates)
        print(f"ngates: {ngates}")

def qlanc(i,data,ds,dlanz=1e-4):
    data = numpy.asarray(data)
    E = data[:,1]
    norm = data[:,2]
    vect = list(numpy.arange(1,i+2,2))
    n = len(vect)

    H = numpy.zeros([n,n],dtype=complex)
    S = numpy.zeros([n,n],dtype=complex)
    j = 0
    k = 0
    for l in vect:
        k = 0
        for lp in vect:
            S[j,k] = norm[(l+lp)//2]**2/(norm[l]*norm[lp])
            H[j,k] = E[(l+lp)//2]*S[j,k]
            k += 1
        j += 1
    # Determine which vectors to keep to stabilize lanczos scheme
    idx = [0]
    ii = 0
    jj = 0
    #'''
    while(ii<n and jj<n-1):
        for jj in range(ii+1,n):
            if(S[ii,jj]<ds):
                idx.append(jj)
                break
        ii = idx[len(idx)-1]
    if S.shape[0]-1 not in idx:
        idx += [S.shape[0]-1]
    #'''
    H_ = H[idx,:]
    H_ = H_[:,idx]
    S_ = S[idx,:]
    S_ = S_[:,idx]
    n = S_.shape[0]

    print('selected QL states:', idx)
    print('states overlap with psi_0:', S[0,0], S[0,-1])
    print(f"H_qlanczos dimension: {n}")

    #'''
    # Regularize the overlap matrix
    sigma,V = numpy.linalg.eigh(S_)
    jdx = numpy.where(sigma>dlanz)[0]
    S_ = numpy.dot(V.T,numpy.dot(S_,V))
    H_ = numpy.dot(V.T,numpy.dot(H_,V))
    S_ = S_[jdx,:]
    S_ = S_[:,jdx]
    H_ = H_[jdx,:]
    H_ = H_[:,jdx]
    #'''
    eps,c = SciLA.eig(H_,S_)
    eps = numpy.sort(eps)
    #return numpy.min(eps)
    #pdb.set_trace()
    return eps



