#!/usr/bin/env python
import model
import ansatz
import avaridyn
import pdb

nsite = 6
ansatz = ansatz.ansatz(nsite, '001001', order = 6,rcut=5.e-4,op_filename='Puccsd_pool_nq6_np4.dat' ,opdense = True) # parity for H4
filename = 'h4trial_r1.5.dat'
model = model.Molecule(nsite,filename )
dyn = avaridyn.avaridynMolecule(model, ansatz, dtmax=0.1, dthmax=0.05)
dyn.run()
