import pdb
import sys
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from matplotlib import rc
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import copy
from scipy import linalg as SciLA
import re

def process_mmd(fname):
    f0 = open(fname,'r')
    dyn_data = []
    mmd_data = []
    while True:
        line = f0.readline().strip()
        if len(line) < 1:
            break
        #print(line)
        if 'initial' in line:
            mmd_data.append(float(line.split()[-1]))
            dyn_data.append([])
        else:
            dyn_data[-1].append(float(line))
            #dyn_data.append(float(line))
            #print(line.split()[-1])

    #print(dyn_data)
    nmax = 0
    for t in dyn_data:
        #print(t)
        n = len(t)
        #print(len(t))
        if n > nmax:
            nmax = n
    print(nmax)
    
    return mmd_data, dyn_data, nmax

def plot_result(eqite_file, eqlanc_file, mmd_file,ansatz_file):

    #ax1 = plt.subplot(1,3,1, adjustable='box', aspect=1.5)
    #ax2 = plt.subplot(1,3,2, adjustable='box', aspect=1.0)
    #ax3 = plt.subplot(1,3,3)
    #fig, axs = plt.subplots(ncols=3, nrows=1, axwidth=1.6, aspect=(3,2))
    fig, axs = plt.subplots(ncols=3, nrows=1)
    ax1 = axs[0]
    ax2 = axs[1]
    ax3 = axs[2]
  
    ax1.text(0.4,-1.84,'a)',fontsize=14)
    ax2.text(-0.7,4.e-2,'b)',fontsize=14)
    ax3.text(0.2,23,'c)',fontsize=14)
    #ax1.set_

    '''
    plot Energy
    '''
    e_exact = -1.9961503198 
    e_qite = np.loadtxt(eqite_file)
    e_qlanc = np.loadtxt(eqlanc_file, dtype = complex)
    ax1.plot(e_qite[:,0],e_qite[:,2],'-o', label = "QITE",markevery=10)
    ax1.plot(e_qlanc[:,0],e_qlanc[:,1],'-s', label = "QLanczos",markevery=20)
    ax1.plot([0,max(e_qite[:,0])], [e_exact, e_exact], 'r--', label = 'Exact')
    ax1.set_ylabel(r'$E$',fontsize=14)
    ax1.set_xlabel(r'$\beta$',fontsize=14)
    #ax1.set_aspect('equal', 'datalim')
    ax1.legend()

    '''
    plot L^2
    '''
    mmd_data, dyn_data, nmax = process_mmd(mmd_file)

    t = np.asarray(range(0,nmax*len(mmd_data),nmax))

    ax2.plot(t*0.01,mmd_data,'^', label = 'AVQITE')
    ct = 0
    for i, dyn in enumerate(dyn_data):
        if len(dyn)>0:
            t0 = np.asarray(range(i*nmax,i*nmax+len(dyn)))
            if ct==0: 
                ax2.plot(t0*0.01+0.01,dyn,'ro', label = "add op", markerfacecolor='none')
            else:
                ax2.plot(t0*0.01+0.01,dyn,'ro', markerfacecolor='none')
            ct += 1
    ax2.set_yscale("log")
    ax2.set_ylabel(r'$L^{2}$', fontsize=14, labelpad=0.01 )
    ax2.set_xlabel(r'$\beta$',fontsize=14)
    ax2.set_xlim(-1,6)
    #ax2.set_aspect('equal')
    ax2.legend()
    #ax2.set_aspect('equal','datalim')

    '''
    Plot N_theta
    '''
    ansatz_data = np.loadtxt(ansatz_file)
    ax3.plot(ansatz_data[:,0],ansatz_data[:,1], lw = 3)
    ax3.set_ylabel(r'$N_{\theta}$',fontsize=14)
    ax3.set_xlabel(r'$\beta$',fontsize=14)
    #ax3.set_aspect('equal','datalim')
    #ax3.set_aspect('equal')
    #ax3.legend()
    fig.subplots_adjust(bottom=0.3, wspace=0.25, top=0.8)
    #plt.tight_layout()
    plt.show(block=False)
    try:
        input("Hit enter to close")
    except:
        plt.close()









if __name__=="__main__":


    eqite_file = 'Energyh4trial_r1.5_mmdN6rcut0.0005_dt0.1.out'
    eqlanc_file = 'EQlanch4trial_r1.5_mmdN6rcut0.0005_dt0.1.out'
    plot_result(eqite_file,eqlanc_file,'mmd_record.dat','ansatz_record.dat')

