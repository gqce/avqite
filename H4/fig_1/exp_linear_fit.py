import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit


def func(x, a, b, c):
    return a * np.exp(-b * x) + c

data = np.loadtxt("Energyh4trial_r1.5_mmdN6rcut0.0005_dt0.1.out")
popt, pcov = curve_fit(func, data[:, 0], data[:, 2])
print(popt)

plt.figure()
plt.plot(data[:, 0], data[:, 2], 'ko')
plt.plot(data[:, 0], func(data[:, 0], *popt), 'r-')
plt.show()

def func_linear(x, a, b):
    return a * x + b

popt, pcov = curve_fit(func_linear, data[:, 1], data[:, 2])
print(popt)

plt.figure()
plt.plot(data[:, 1], data[:, 2], 'ko')
plt.plot(data[:, 1], func_linear(data[:, 1], *popt), 'r-')
plt.show()

