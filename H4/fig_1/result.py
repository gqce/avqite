import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid.inset_locator import InsetPosition


def process_mmd(fname, dt=0.1):
    f0 = open(fname, 'r')
    d_list = []
    t_list = []
    nt = -1
    for line in f0:
        line = line.split()
        if len(line) < 1:
            continue
        elif 'initial' in line:
            nadd = len(d_list) - len(t_list)
            if nadd > 0:
                ddt = dt/2/nadd
                for i in range(nadd):
                    t_list.append(t_list[-1] + ddt)
            nt += 1
            t_list.append(nt*dt)
        d_list.append(float(line[-1]))
    assert(len(d_list) == len(t_list)), "lists of t, d inconsistent."
    return t_list, d_list


def plot_result(eqite_file, eqlanc_file, mmd_file, ansatz_file):

    fig, axs = plt.subplots(ncols=3, nrows=1, figsize=(7.5, 3.5), sharex=True)
    ax1, ax2, ax3 = axs

    ax1.text(0.1, 0.9, '(a)', transform=ax1.transAxes)
    ax1.text(0.5, 0.5, r'H$_4$', transform=ax1.transAxes)
    ax2.text(0.1, 0.9, '(b)', transform=ax2.transAxes)
    ax3.text(0.3, 0.9, '(c)', transform=ax3.transAxes)
    ax3.text(2.0, 8e-4, r'$L^2_{cut}$')

    '''
    plot Energy
    '''
    e_exact = -1.9961503198
    e_qite = np.loadtxt(eqite_file)
    e_qlanc = np.loadtxt(eqlanc_file, dtype=complex).real
    ax1.plot(e_qite[:, 0], e_qite[:, 2], '-o', label="AVQITE",
            lw=1, ms=3)
    ax1.plot(e_qlanc[:, 0], e_qlanc[:, 1], '-s', label="QLanczos",
            lw=1, ms=3)
    ax1.axhline(y=e_exact, ls="--", label="Exact")
    ax1.set_ylabel('E (Ha)')
    ax1.set_xlabel(r'$\tau$')
    ax1.set_xlim(-0.2, 5)
    ax1.legend()

    '''
    Plot N_theta
    '''
    ansatz_data = np.loadtxt(ansatz_file)
    ax2.plot(ansatz_data[:, 0], ansatz_data[:, 1])
    ax2.set_ylabel(r'$N_{\mathbf{\theta}}$')
    ax2.set_xlabel(r'$\tau$')

    # Create a set of inset Axes:
    # these should fill the bounding box allocated to them.
    ax4 = plt.axes([0,0,1,1])
    # Manually set the position and relative size of the inset axes within ax1
    ip = InsetPosition(ax2, [0.23,0.1,0.5,0.6])
    ax4.set_axes_locator(ip)
    #ax4.set_xlabel(r"$\tau$")
    #ax4.set_ylabel("error (kcal/mol)")
    ax4.text(0.8, 0.2, "error (kcal/mol)", transform=ax4.transAxes,
            rotation=90)
    ax4.plot(e_qite[:, 0], (e_qite[:, 2]-e_exact)*627.5, "-o",
            lw=0.5, ms=2)
    ax4.plot(e_qlanc[:, 0], (e_qlanc[:, 1]-e_exact)*627.5, "-s",
            lw=0.5, ms=2)
    ax4.yaxis.tick_right()
    x0, x1 = e_qite[0, 0]-0.3, e_qite[-1, 0] +0.3
    ax4.axhline(y=1, ls="--", color="gray")
    ax4.fill_between([x0, x1], [1, 1], color='lightgray')
    ax4.set_xlim(x0, x1)
    ax4.set_ylim(0.08, 100)
    ax4.set_yscale("log")

    '''
    plot L^2
    '''
    t_list, d_list = process_mmd(mmd_file)
    ax3.plot(t_list, d_list, '-o', lw=1, markersize=3, label=r"$L^2$")
    ax3.plot(e_qite[:, 0], e_qite[:, 1], 'r-+', lw=1, markersize=3,
            label=r"var$_{\mathbf{\theta}}[\hat{\mathcal{H}}]$")
    ax3.axhline(y=5e-4, ls="--", color="gray")
    ax3.fill_between([-0.2, 5], [5e-4, 5e-4], color='lightgray')
    ax3.set_yscale("log")
    # ax3.set_ylabel(r'$L^{2}$')
    ax3.set_xlabel(r'$\tau$')
    ax3.legend()

    plt.tight_layout()
    plt.show()
    fig.savefig("avqite_h4.pdf")



if __name__ == "__main__":

    eqite_file = 'Energyh4trial_r1.5_mmdN6rcut0.0005_dt0.1.out'
    eqlanc_file = 'EQlanch4trial_r1.5_mmdN6rcut0.0005_dt0.1.out'
    plot_result(eqite_file, eqlanc_file, 'mmd_record.dat', 'ansatz_record.dat')
