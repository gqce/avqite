#!/usr/bin/env python
import model
import ansatz
import avaridyn
import pdb

nsite = 6
ansatz = ansatz.ansatz(nsite, '000000', order = 6,rcut=5.0e-4,\
         op_filename ='N6maxpool.dat', opdense = True) 
filename = 'N6_J-1.0_h1.0.dat'

model = model.Ising(nsite,filename )
dyn = avaridyn.avaridynIsing(model, ansatz, dtmax=0.1, dthmax=0.05)
dyn.run()
