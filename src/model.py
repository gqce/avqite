import numpy as np
from qutip import qeye, sigmax, sigmay, sigmaz, tensor, Qobj
import pdb
import re
from numpy import linalg as LA



class Molecule:
    def __init__(self, nqbit, filename):
        self._nqbit = nqbit
        self._filename = filename
        self._evolution = 'I' # Imaginary time evolution
        self._sx_list, self._sy_list, self._sz_list = get_sxyz_ops(self._nqbit)
        #self.hamil = self.set_terms()
        self._h = self.set_terms()
        self._t = None
        self._T = 2
        self._opdense = True

    def set_terms(self):
        '''
        read Hamiltonian
        '''
        f0 = open(self._filename)
        Hamil = 0
        while True:
            line = f0.readline().strip().split('*')
            #line = f0.readline().split()
            #print(line)
            if len(line) < 2:
                break
            #if (abs(complex(line[0]))) < 1e-6:
            if (abs(float(line[0]))) < 1e-6:
                continue
            #if len(line) < 2:
            #    term = np.eye(2**self._nqbit)
            #    Hamil += np.asarray(np.real(complex(line[0]))*term)
            else:
                pauli_string = line[-1].strip()
                #term = get_pauli_terms(pauli_string, 6)
                tmp = re.findall(r'\d',pauli_string)
                active_bits = list(map(int, tmp))
                tmp = re.findall('[A-Z]',pauli_string)
                active_gates = tmp
                #print(active_bits, active_gates)
                term = 1
                for i,g in enumerate(active_gates):
                    if g=='I':
                        term *= np.eye(2**self._nqbit)
                    if g=='X':
                        term *= self._sx_list[active_bits[i]]
                    if g=='Y':
                        term *= self._sy_list[active_bits[i]]
                    if g=='Z':
                        term *= self._sz_list[active_bits[i]]
                #print(term)
                #pdb.set_trace()
                #Hamil = Hamil +  np.asarray(np.real(complex(line[0]))*term)
                Hamil = Hamil +  np.asarray(np.real(float(line[0]))*term)
                #print(Hamil)
                #pdb.set_trace()

        f0.close()
        print("Constructed Hamiltonian")
        #print(Hamil)
        if isinstance(Hamil, np.ndarray):
            Hamil = Qobj(Hamil)
        # =Hamil
        return Hamil

    def get_evolution(self):
        return self._evolution

    def set_sops(self):
        '''set up site-wise sx, sy, sz operators.
        '''
        self._sx_list, self._sy_list, self._sz_list = get_sxyz_ops(self._nsite)

    def get_loweste_states(self, t):
        #self.set_h(t)
        w, v = self._h.eigenstates(eigvals=3)
        #w = self._h.eigenenergies(sparse=False, sort='low', eigvals=3, tol=0)
        return w, v

    def get_h_expval(self, t, vec):
        #self.set_h(t)
        if isinstance(vec, np.ndarray):
            vec = Qobj(vec)
        return self._h.matrix_element(vec, vec).real

    def get_h(self, t):
        #self.set_h(t)

        return self._h

    #def set_h(self, t):
        #self._h = self.set_terms()
        #if self._t is None or abs(self._t-t) > 1e-7:
        #    if t > self._T:
        #        t = self._T
        #    self._t = t
        #self._h = self.hamil

    def get_evolution(self):
        return self._evolution


class Ising:
    def __init__(self, nqbit, filename):
        self._nqbit = nqbit
        self._filename = filename
        self._evolution = 'I' # Imaginary time evolution
        self._sx_list, self._sy_list, self._sz_list = get_sxyz_ops(self._nqbit)
        #self.hamil = self.set_terms()
        self._h = self.set_terms()
        self._t = None
        self._T = 2
        self._opdense = True

    def set_terms(self):
        '''
        read Hamiltonian
        '''
        f0 = open(self._filename)
        Hamil = 0
        while True:
            line = f0.readline().strip().split('*')
            #line = f0.readline().split()
            #print(line)
            if len(line) < 2:
                break
            #if (abs(complex(line[0]))) < 1e-6:
            if (abs(float(line[0]))) < 1e-6:
                continue
            #if len(line) < 2:
            #    term = np.eye(2**self._nqbit)
            #    Hamil += np.asarray(np.real(complex(line[0]))*term)
            else:
                pauli_string = line[-1].strip()
                #term = get_pauli_terms(pauli_string, 6)
                tmp = re.findall(r'\d',pauli_string)
                active_bits = list(map(int, tmp))
                tmp = re.findall('[A-Z]',pauli_string)
                active_gates = tmp
                #print(active_bits, active_gates)
                term = 1
                for i,g in enumerate(active_gates):
                    if g=='I':
                        term *= np.eye(2**self._nqbit)
                    if g=='X':
                        term *= self._sx_list[active_bits[i]]
                    if g=='Y':
                        term *= self._sy_list[active_bits[i]]
                    if g=='Z':
                        term *= self._sz_list[active_bits[i]]
                #print(term)
                #pdb.set_trace()
                #Hamil = Hamil +  np.asarray(np.real(complex(line[0]))*term)
                Hamil = Hamil +  np.asarray(np.real(float(line[0]))*term)
                #print(Hamil)
                #pdb.set_trace()

        f0.close()
        print("Constructed Hamiltonian")
        #print(Hamil)
        if isinstance(Hamil, np.ndarray):
            Hamil = Qobj(Hamil)
        # =Hamil
        return Hamil

    def get_evolution(self):
        return self._evolution

    def set_sops(self):
        '''set up site-wise sx, sy, sz operators.
        '''
        self._sx_list, self._sy_list, self._sz_list = get_sxyz_ops(self._nsite)

    def get_loweste_states(self, t):
        #self.set_h(t)
        w, v = self._h.eigenstates(eigvals=3)
        #w = self._h.eigenenergies(sparse=False, sort='low', eigvals=3, tol=0)
        #w = self._h.eigenenergies(sparse=True, sort='low', tol=0)
        return w, v

    def get_h_expval(self, t, vec):
        #self.set_h(t)
        if isinstance(vec, np.ndarray):
            vec = Qobj(vec)
        return self._h.matrix_element(vec, vec).real

    def get_h(self, t):
        #self.set_h(t)

        return self._h

    #def set_h(self, t):
        #self._h = self.set_terms()
        #if self._t is None or abs(self._t-t) > 1e-7:
        #    if t > self._T:
        #        t = self._T
        #    self._t = t
        #self._h = self.hamil

    def get_evolution(self):
        return self._evolution



class spinModel:
    def __init__(self, nsite):
        self._nsite = nsite
        self._evolution = 'R' # Real time evolution
        self.set_sops()

    def set_sops(self):
        '''set up site-wise sx, sy, sz operators.
        '''
        self._sx_list, self._sy_list, self._sz_list = get_sxyz_ops(self._nsite)

    def get_loweste_states(self, t):
        #self.set_h(t)
        w, v = self._h.eigenstates(eigvals=3)
        return w, v

    def get_h_expval(self, t, vec):
        #self.set_h(t)
        if isinstance(vec, np.ndarray):
            vec = Qobj(vec)
        pdb.set_trace()
        return self._h.matrix_element(vec, vec).real

    def get_h(self, t):
        self.set_h(t)
        return self._h


    def set_h(self, t):
        if self._t is None or abs(self._t-t) > 1e-7:
            if t > self._T:
                t = self._T
            gamma = self._gi + (self._gf - self._gi)*t/self._T
            jx, jy = -self._J*(1 + gamma), -self._J*(1 - gamma)
            h = jx*self._xx + jy*self._yy + self._hz*self._z
            self._t = t
            self._h = h

    def get_evolution(self):
        return self._evolution

class xyzmodel(spinModel):
    def __init__(self, nsite=2, T=0.5, gi=1, gf=-1, hz=-0.7, J=1):
        super().__init__(nsite)
        self._T = T
        self._gi = gi
        self._gf = gf
        self._hz = hz
        self._J = J
        self._t = None
        self._corr_ops = None  # spin-correlation operators
        self.set_terms()

    def set_terms(self):
        self._xx, self._yy, self._z = 0, 0, self._sz_list[-1]
        for i in range(self._nsite-1):
            self._xx += self._sx_list[i]*self._sx_list[i+1]
            self._yy += self._sy_list[i]*self._sy_list[i+1]
            self._z += self._sz_list[i]

    @property
    def corr_ops(self):
        if self._corr_ops is None:
            self.setup_corr_ops()
        return self._corr_ops

    def setup_corr_ops(self):
        # sx[0]sx[1]
        self._corr_ops = [
                (self._sx_list[0]*self._sx_list[1]),
                (self._sx_list[0]*self._sx_list[-1]),
                (self._sy_list[0]*self._sy_list[1]),
                (self._sy_list[0]*self._sy_list[-1]),
                ]

    def set_h(self, t):
        if self._t is None or abs(self._t-t) > 1e-7:
            if t > self._T:
                t = self._T
            gamma = self._gi + (self._gf - self._gi)*t/self._T
            jx, jy = -self._J*(1 + gamma), -self._J*(1 - gamma)
            h = jx*self._xx + jy*self._yy + self._hz*self._z
            self._t = t
            self._h = h

    def h_terms(self, t):
        if t > self._T:
            t = self._T
        gamma = self._gi + (self._gf - self._gi)*t/self._T
        jx, jy = -self._J*(1 + gamma), -self._J*(1 - gamma)
        hz = self._hz
        for i in range(self._nsite):
            yield hz*self._sz_list[i]
        for i in range(self._nsite-1):
            yield jx*self._sx_list[i]*self._sx_list[i+1]
        for i in range(self._nsite-1):
            yield jy*self._sy_list[i]*self._sy_list[i+1]



class isingmodel(spinModel):
    def __init__(self, nsite=2, T=1.6, hx=-2.0, J=1, T1=0.5, mode=0):
        super().__init__(nsite)
        self._hx = hx
        self._J = J
        self._T = T
        self._T1 = T1
        self._t = None
        self._mode = mode
        self.set_terms()

    def set_terms(self):
        self._x, self._zz = 0, 0
        # periodic boundary condition
        for i in range(-1, self._nsite-1):
            self._x += self._sx_list[i]
            self._zz += self._sz_list[i]*self._sz_list[i+1]

    def set_h(self, t):
        if self._t is None or abs(self._t-t) > 1e-7:
            if self._mode == 0:
                if t < 1.e-6:
                    if self._t > 0:
                        self._h = -self._zz
                        self._t = -1
                else:
                    if self._t < 0:
                        self._h = -self._zz + self._hx*self._x
                        self._t = 1
            else:
                if t < self._T1:
                    hx = t/self._T1*self._hx
                else:
                    hx = self._hx
                self._h = -self._zz + hx*self._x
                self._t = t



def get_sxyz_ops(nsite):
    '''set up site-wise sx, sy, sz operators.
    '''
    si = qeye(2)
    sx = sigmax()
    sy = sigmay()
    sz = sigmaz()
    sx_list = []
    sy_list = []
    sz_list = []

    op_list = [si for i in range(nsite)]
    for i in range(nsite):
        op_list[i] = sx
        sx_list.append(tensor(op_list))
        op_list[i] = sy
        sy_list.append(tensor(op_list))
        op_list[i] = sz
        sz_list.append(tensor(op_list))
        # reset
        op_list[i] = si
    return [sx_list, sy_list, sz_list]


def make_full_string(active_bits, active_gates,Nq):
    bstring = ['I']*Nq
    for i in range(len(active_bits)):
        bstring[ active_bits[i] ] = active_gates[i]
    return bstring

def get_pauli_terms(pauli_string, Nq):

    '''
    reads the Pauli string and returns
    them in matrix form
    '''

    si = qeye(2)
    sx = sigmax()
    sy = sigmay()
    sz = sigmaz()

    operator_dict = {}
    operator_dict['I'] = si
    operator_dict['X'] = sx
    operator_dict['Y'] = sy
    operator_dict['Z'] = sz

    sx_list, sy_list, sz_list = get_sxyz_ops(Nq)

    #pdb.set_trace()
    print(pauli_string)
    tmp = re.findall(r'\d',pauli_string)
    active_bits = list(map(int, tmp))
    tmp = re.findall('[A-Z]',pauli_string)
    active_gates = tmp
    print(active_bits, active_gates)
    #active_gates = [  gate_dict.get_num(i) for i in list(tmp)  ]
    bstring = make_full_string(active_bits, active_gates, Nq)

    term = si
    for t in bstring:
        term *= operator_dict[t]
    print(bstring)
    #print(tmp)
    #pdb.set_trace()











