import sys
import os
import numpy as np
import matplotlib.pyplot as plt


data00 = np.loadtxt("data_tfim_pmref.dat")
data01 = np.loadtxt("data_tfim_fmref.dat")
data10 = np.loadtxt("data_mfim_fmref.dat")

fig, axs = plt.subplots(ncols=1, nrows=1, figsize=(4, 3))

#HVA TFIM data, L**2
data_ref00 = [list(range(2,11,2))]
data_ref00.append([x**2 for x in data_ref00[0]])
data_ref01 = [np.linspace(2, 10, 100)]
data_ref01.append(data_ref01[0]**2)

axs.scatter(data00[:,0], data00[:,3]*2, marker='o', color='r',
        label = "AVQITE")
axs.scatter(data_ref00[0], data_ref00[1], marker='*', color='r',
        label = "HVA")
axs.plot(data_ref01[0], data_ref01[1], ':', color='r')

#HVA MFIM data, 4L.
data_ref = [list(range(2,11))]
data_ref.append([x*4 for x in data_ref[0]])
axs.plot(data10[:,0], data10[:,3]*2, 'o:', color='b')
axs.plot(data_ref[0], data_ref[1], '*:', color='b')
axs.set_xlim(3.8, 10.2)
axs.set_xlabel(r'$N$')
axs.set_ylabel('Number of CNOTs')

axs.text(0.6, 0.7, r"~$N^2$", transform=axs.transAxes)
axs.text(0.8, 0.4, r"~$4N$", transform=axs.transAxes)
axs.text(0.8, 0.25, r"~$2N$", transform=axs.transAxes)
axs.text(0.3, 0.5, "TFIM", color='r', transform=axs.transAxes)
axs.text(0.7, 0.1, "MFIM", color='b', transform=axs.transAxes)

axs.legend()


plt.tight_layout()
plt.show()
fig.savefig('scaling.pdf')
