# avqite

AVQITE code for the manuscript ``Adaptive Variational Quantum Imaginary Time Evolution for Quantum Chemistry''.

[![Paper](https://img.shields.io/badge/paper-arXiv%3A2102.01544-B31B1B.svg)](https://arxiv.org/abs/2102.01544)


# Adaptive Variational Quantum Imaginary Time Evolution Approach for Quantum Chemistry Calculations 
Niladri Gomes, Anirban Mukherjee, Feng Zhang, Thomas Iadecola, Cai-Zhuang Wang, Kai-Ming Ho, Peter P Orth, Yong-Xin Yao

### Description
This repository includes code and data for the adaptive variational quantum imaginary time evolution method developed in [arXiv:2102.01544](https://arxiv.org/abs/2102.01544).
Source code is included in `/src`
For a sample run: `python run.py`

### Requirements
  - python=3.7
  - matplotlib
  - numpy
  - scipy
  - [QUTIP](https://qutip.org/download.html)

