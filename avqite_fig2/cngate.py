import pdb
import sys
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from matplotlib import rc
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import copy
from scipy import linalg as SciLA
import re

def find_cn(fname):
    r = eval(fname.split('_')[1][1:])
    dt = eval(fname.split('_')[-1][2:-4])
    print(dt)
    t = 0
    f0 = open(fname,'r')
    ngate_data = []
    while True:
        line = f0.readline().strip()
        if len(line)<1:
            break
        if 'ngate' in line:
            
            term = eval(line.split('ngates:')[1])
            num_cn = sum([2*(l-1)*t for l,t in enumerate(term)])
            num_op = sum(term)
            #print(line.split('ngates:'))
            #print(term)
            print(num_cn)
            ngate_data.append([t,num_cn,num_op])
            t += dt
    f0.close()
    np.savetxt("h2o_ngater"+str(r)+".dat",np.asarray(ngate_data))
 
if __name__=="__main__":
    fname = sys.argv[1]
    find_cn(fname)
