import itertools
import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate


def plot_result():

    fig, axs = plt.subplots(ncols=3, nrows=3, figsize=(7.5, 6.0))
    axs[0,0].text(0.4, 0.35, r'H$_4$', transform=axs[0,0].transAxes)
    axs[0,1].text(0.4, 0.45, r'H$_2$O', transform=axs[0,1].transAxes)
    axs[0,2].text(0.4, 0.45, r'BeH$_2$', transform=axs[0,2].transAxes)

    # fig
    # add H4 molecule
    h4x = np.linspace(0.4*2.4, 0.75*2.4, num=4)
    h4y = [-1.6]*4
    axs[0,0].plot(h4x, h4y, "-", color="brown", zorder=1)
    axs[0,0].scatter(h4x, h4y, s=50, marker="o", color="green", zorder=2)

    # add H2O molecule
    h2ox = [1.5-0.2, 1.5, 1.5+0.2]
    h2oy = [-74.45, -74.35, -74.45]
    axs[0,1].plot(h2ox, h2oy, "-", color="brown", zorder=1)
    axs[0,1].scatter(h2ox[::2], h2oy[::2], s=50, marker="o", color="green",
            zorder=2)
    axs[0,1].scatter(h2ox[1::2], h2oy[1::2], s=100, marker="o", color="tomato",
            zorder=2)

    # add BeH2 molecule
    beh2x = [2.5-0.5, 2.5, 2.5+0.5]
    beh2y = [-14.75]*3
    axs[0,2].plot(beh2x, beh2y, "-", color="brown", zorder=1)
    axs[0,2].scatter(beh2x[::2], beh2y[::2], s=50, marker="o",
            color="green", zorder=2)
    axs[0,2].scatter(beh2x[1::2], beh2y[1::2], s=150, marker="o",
            color="indigo", zorder=2)


    #labels
    from string import ascii_lowercase
    for k, ij in enumerate(itertools.product(range(3), repeat=2)):
        i, j = ij
        axs[i, j].text(0.1, 0.8, f"({ascii_lowercase[k]})",
                transform=axs[i, j].transAxes)


    '''
    plot dissociation curve
    '''
    ex_file = "h4_exact.dat"
    e_exact = np.loadtxt(ex_file)
    e_qite = np.loadtxt('h4_qite.dat')
    f = interpolate.interp1d(e_exact[:,0], e_exact[:,1], kind='cubic')
    rlist = np.linspace(e_exact[0,0], e_exact[-1,0], 50)
    elist = f(rlist)
    axs[0,0].plot(rlist, elist, '-', label = "Exact")
    axs[0,0].scatter(e_qite[:,0],e_qite[:,1],marker='o', c=["r", "g", "b"],
            label = "AVQITE")
    axs[0,0].set_ylabel('E(Ha)')
    axs[0,0].set_xlabel(r'$R_{HH}(\AA)$')
    axs[0,0].legend()

    ex_file = "h2o_exact.dat"
    e_exact = np.loadtxt(ex_file)
    e_qite = np.loadtxt('h2o_qite.dat')
    f = interpolate.interp1d(e_exact[:,0], e_exact[:,1], kind='cubic')
    rlist = np.linspace(e_exact[0,0], e_exact[-1,0], 50)
    elist = f(rlist)
    axs[0,1].plot(rlist, elist, '-', label = "Exact")
    axs[0,1].scatter(e_qite[:,0],e_qite[:,1],marker='o', c=["r", "g", "b"],
            label = "AVQITE")
    axs[0,1].set_xlabel(r'$R_{HO}(\AA)$')

    ex_file = "beh2_exact.dat"
    e_exact = np.loadtxt(ex_file)
    e_qite = np.loadtxt('beh2_qite.dat')
    f = interpolate.interp1d(e_exact[:,0], e_exact[:,1], kind='cubic')
    rlist = np.linspace(e_exact[0,0], e_exact[-1,0], 50)
    elist = f(rlist)
    axs[0,2].plot(rlist, elist, '-', label = "Exact")
    axs[0,2].scatter(e_qite[:,0],e_qite[:,1],marker='o', c=["r", "g", "b"],
            label = "AVQITE")
    axs[0,2].set_xlabel(r'$R_{BeH}(\AA)$')
    '''
    plot error
    '''
    nthetas = np.loadtxt("nthetas.dat").T

    e1 = np.loadtxt("Energyh4trial_r0.8N6rcut0.0001_dt0.1.out")
    e1ex = -2.167560544134054588
    e2 = np.loadtxt("Energyh4trial_r1.5N6rcut0.0005_dt0.1.out")
    e2ex = -1.996150325518806001
    e3 = np.loadtxt("Energyh4trial_r2.4N6rcut0.0005_dt0.5.out")
    e3ex = -1.874651582485890922
    axs[1,0].plot(e1[:,0],(e1[:,2]-e1ex)*627.5, 'r-', label = "r=0.8" )
    axs[1,0].plot(e2[:,0],(e2[:,2]-e2ex)*627.5, 'g-', label = "r=1.5" )
    axs[1,0].plot(e3[:,0],(e3[:,2]-e3ex)*627.5, 'b-', label = "r=2.4" )

    dx = (e3[-1, 0] - e3[0, 0])*0.02
    x0, x1 = e3[0, 0] - dx, e3[-1, 0] + dx
    axs[1,0].axhline(y=1, ls="--", color="gray")
    axs[1,0].fill_between([x0, x1], [1, 1], color='lightgray')
    axs[1,0].set_xlim(x0, x1)
    axs[1,0].set_ylabel('Error (kcal/mol)')
    axs[1,0].set_yscale("log")
    axs[1,0].set_yticks([0.1, 1, 10, 100])
    axs[1,0].set_ylim(0.08, 600)

    ax0 = axs[1, 0].inset_axes([0.58,0.55,0.4,0.4])
    ax0.set_xlabel("ADAPT-VQE", fontsize=9)
    ax0.set_ylabel("AVQITE", fontsize=9)
    ax0.text(0.2, 0.7, r"$N_{\theta}$", transform=ax0.transAxes)
    ax0.scatter(nthetas[0][:2], nthetas[3][:2], c=["r", "g"])
    ax0.scatter(nthetas[0][2:], nthetas[3][2:], marker="X", color="b")
    ax0.plot([10, 60], [10, 60], "--", color="gray")
    ax0.set_xlim(10, 60)
    ax0.set_ylim(10, 60)
    ax0.xaxis.set_tick_params(direction="in", labelsize=9)
    ax0.yaxis.set_tick_params(direction="in", labelsize=9)


    e1 = np.loadtxt("Energyh2otrial_r0.8_parityN10rcut0.0001_dt0.1.out")
    e1ex = -74.86918880666920018
    e2 = np.loadtxt("Energyh2otrial_r1.5_parityN10rcut0.0001_dt0.1.out")
    e2ex = -74.86410464384658781
    e3 = np.loadtxt("Energyh2otrial_r2.4_parityN10rcut0.0001_dt0.1.out")
    e3ex = -74.7404824929
    axs[1,1].plot(e1[:,0],(e1[:,2]-e1ex)*627.5, 'r-', label = "r=0.8" )
    axs[1,1].plot(e2[:,0],(e2[:,2]-e2ex)*627.5, 'g-', label = "r=1.5" )
    axs[1,1].plot(e3[:,0],(e3[:,2]-e3ex)*627.5, 'b-', label = "r=2.4" )

    dx = (e3[-1, 0] - e3[0, 0])*0.02
    x0, x1 = e3[0, 0] - dx, e3[-1, 0] + dx
    axs[1,1].axhline(y=1, ls="--", color="gray")
    axs[1,1].fill_between([x0, x1], [1, 1], color='lightgray')
    axs[1,1].set_xlim(x0, x1)
    axs[1,1].set_yscale("log")
    axs[1,1].set_yticks([0.1, 1, 10, 100])
    axs[1,1].set_ylim(0.08, 600)

    ax1 = axs[1, 1].inset_axes([0.58,0.55,0.4,0.4])
    ax1.set_xlabel("ADAPT-VQE", fontsize=9)
    ax1.set_ylabel("AVQITE", fontsize=9)
    #ax1.text(0.45, 0.7, r"$N_{\theta}$", transform=ax1.transAxes)
    ax1.scatter(nthetas[1], nthetas[4], c=["r", "g", "b"])
    ax1.plot([10, 60], [10, 60], "--", color="gray")
    ax1.set_xlim(10, 60)
    ax1.set_ylim(10, 60)
    ax1.xaxis.set_tick_params(direction="in", labelsize=9)
    ax1.yaxis.set_tick_params(direction="in", labelsize=9)


    e1 = np.loadtxt("Energybeh2_r0.8_parityN8rcut0.0001_dt0.05.out")
    e1ex =  -1.516806700866867530e+01
    e2 = np.loadtxt("Energybeh2_r1.4_parityN8rcut0.0001_dt0.05.out")
    e2ex =  -1.558785437941308061e+01
    e3 = np.loadtxt("Energybeh2_r3.6_parityN8rcut0.0005_dt0.5.out")
    e3ex = -1.532253372042569950e+01
    axs[1,2].plot(e1[:,0],(e1[:,2]-e1ex)*627.5, 'r-', label = "r=0.8" )
    axs[1,2].plot(e2[:,0],(e2[:,2]-e2ex)*627.5, 'g-', label = "r=1.4" )
    axs[1,2].plot(e3[:,0],(e3[:,2]-e3ex)*627.5, 'b-', label = "r=3.6" )

    dx = (e3[-1, 0] - e3[0, 0])*0.02
    x0, x1 = e3[0, 0] - dx, e3[-1, 0] + dx
    axs[1,2].axhline(y=1, ls="--", color="gray")
    axs[1,2].fill_between([x0, x1], [1, 1], color='lightgray')
    axs[1,2].set_xlim(x0, x1)
    axs[1,2].set_yscale("log")
    axs[1,2].set_yticks([0.1, 1, 10, 100])
    axs[1,2].set_ylim(0.08, 600)

    ax2 = axs[1, 2].inset_axes([0.58,0.55,0.4,0.4])
    ax2.set_xlabel("ADAPT-VQE", fontsize=9)
    ax2.set_ylabel("AVQITE", fontsize=9)
    #ax2.text(0.45, 0.7, r"$N_{\theta}$", transform=ax2.transAxes)
    ax2.scatter(nthetas[2], nthetas[5], c=["r", "g", "b"])
    ax2.plot([10, 60], [10, 60], "--", color="gray")
    ax2.set_xlim(10, 60)
    ax2.set_ylim(10, 60)
    ax2.xaxis.set_tick_params(direction="in", labelsize=9)
    ax2.yaxis.set_tick_params(direction="in", labelsize=9)

    '''
    Plot N_theta
    '''

    cn1_data = np.vstack(([-0.1, 0, 0], np.loadtxt("h4_ngater0.8.dat")))
    cn2_data = np.vstack(([-0.1, 0, 0], np.loadtxt("h4_ngater1.5.dat")))
    cn3_data = np.vstack(([-0.1, 0, 0], np.loadtxt("h4_ngater2.4.dat")))
    cn1_data = np.vstack((cn1_data, [cn3_data[-1,0], cn1_data[-1,1], cn1_data[-1,2]]))
    cn2_data = np.vstack((cn2_data, [cn3_data[-1,0], cn2_data[-1,1], cn2_data[-1,2]]))

    axs[2,0].plot(cn1_data[:,0],cn1_data[:,1], 'r-', label = "r=0.8")
    axs[2,0].plot(cn2_data[:,0],cn2_data[:,1], 'g-', label = "r=1.5")
    axs[2,0].plot(cn3_data[:,0],cn3_data[:,1], 'b-', label = "r=3.4")
    axs[2,0].set_ylabel('number of CNOTs')
    axs[2,0].set_xlabel(r'$\tau$')

    dx = (5 - cn3_data[0, 0])*0.02
    x0, x1 = cn3_data[0, 0] - dx, 5 + dx
    axs[2,0].set_xlim(x0, x1)
    axs[2,0].set_ylim(0, 450)

    cn1_data = np.vstack(([-0.1, 0, 0], np.loadtxt("h2o_ngater0.8.dat")))
    cn2_data = np.vstack(([-0.1, 0, 0], np.loadtxt("h2o_ngater1.5.dat")))
    cn3_data = np.vstack(([-0.1, 0, 0], np.loadtxt("h2o_ngater2.4.dat")))
    cn1_data = np.vstack((cn1_data, [cn3_data[-1,0], cn1_data[-1,1], cn1_data[-1,2]]))
    cn2_data = np.vstack((cn2_data, [cn3_data[-1,0], cn2_data[-1,1], cn2_data[-1,2]]))

    #cn3_data = np.loadtxt("h2o_ngater2.2.dat")
    axs[2,1].plot(cn1_data[:,0],cn1_data[:,1], 'r-', label = "r=0.8")
    axs[2,1].plot(cn2_data[:,0],cn2_data[:,1], 'g-', label = "r=1.5")
    axs[2,1].plot(cn3_data[:,0],cn3_data[:,1], 'b-', label = "r=2.4")
    axs[2,1].set_xlabel(r'$\tau$')

    dx = (5 - cn3_data[0, 0])*0.02
    x0, x1 = cn3_data[0, 0] - dx, 5 + dx
    axs[2,1].set_xlim(x0, x1)
    axs[2,1].set_ylim(0, 450)

    cn1_data = np.vstack(([-0.1, 0, 0], np.loadtxt("beh2_ngater0.8.dat")))
    cn2_data = np.vstack(([-0.1, 0, 0], np.loadtxt("beh2_ngater1.4.dat")))
    cn3_data = np.vstack(([-0.1, 0, 0], np.loadtxt("beh2_ngater3.6.dat")))
    cn1_data = np.vstack((cn1_data, [cn3_data[-1,0], cn1_data[-1,1], cn1_data[-1,2]]))
    cn2_data = np.vstack((cn2_data, [cn3_data[-1,0], cn2_data[-1,1], cn2_data[-1,2]]))

    axs[2,2].plot(cn1_data[:,0],cn1_data[:,1], 'r-', label = "r=0.8")
    axs[2,2].plot(cn2_data[:,0],cn2_data[:,1], 'g-', label = "r=1.4")
    axs[2,2].plot(cn3_data[:,0],cn3_data[:,1], 'b-', label = "r=3.6")

    dx = (5 - cn3_data[0, 0])*0.02
    x0, x1 = cn3_data[0, 0] - dx, 5 + dx
    axs[2,2].set_xlim(x0, x1)
    axs[2,2].set_ylim(0, 450)

    axs[2,2].set_xlabel(r'$\tau$')
    plt.tight_layout()
    plt.show()
    fig.savefig("avqite_all.pdf")


if __name__=="__main__":
    plot_result()

