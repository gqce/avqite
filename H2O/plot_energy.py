import pdb
import sys
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from matplotlib import rc
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import copy
from scipy import linalg as SciLA
import re


def poly_fit(data):
    n = len(data)
    print(n)
    data_ = copy.deepcopy(data)
    #data = data[int(9*n/10):,:]
    #data_ = data[-50:,:]

    print(data_.shape)
    #x = data_[-5:,1]
    #y = data_[-5:,2]

    x = data_[:,1]
    y = data_[:,2]
    #y = numpy.log(data_[:,1])
    
    #y = A + B log x
    ans = np.polyfit(x, y, 1)
    A = ans[0]
    B = ans[1]
    return A,B




def plot_energy(e_exact,fname1, fname2):
    energy_data = np.loadtxt(fname1)
    eq_data = np.loadtxt(fname2,dtype=complex)
    A, B = poly_fit(energy_data)
    print(f"Slope = {A:.2f} ; Intercept = {B:.2f}")
    #print(f"Extrapolated energy: {y0:.10f}")
    print(f"Extrapolated energy: {B:.10f}")
    print(f"error: {B-e_exact:.10f}")
    param = fname1.split('_')
    r = param[1][1:4]
    rcut = param[2].split('rcut')[1] 
    dt = param[3][2:-4]
    
    title = 'r='+r+', dt='+dt+r'$, r_{cut}$='+rcut
    x1 = 0.1*max(energy_data[:,0])
    x0 = min(np.log(energy_data[:,0]))
    x1 = 0.1*max(np.log(energy_data[:,0]))


    x0 = min(energy_data[:,0])
    x1 = max(energy_data[:,0])
    x_e = np.linspace(0,x1)
    #y_e = A*numpy.log(x_e) + B
    y_e = A*(x_e) + B
    plt.plot(energy_data[:,0],energy_data[:,2]-e_exact, '.', label = "QITE" )
    plt.plot(eq_data[0:,0],eq_data[0:,1]-e_exact, '*--',label = "qLanc" )
    #plt.plot(x_e, y_e, 'k--', lw=2, label = 'Best fit')
    #plt.plot(x_e, y_e, 'k--', lw=2, label = 'Best fit')
    #plt.plot(numpy.log(energy_data[:,0]),energy_data[:,1], '.' )
    #plt.xlabel(r"$var(H)$")
    plt.xlabel(r"$\tau$")
    plt.ylabel(r"$E$")
    plt.title(title)
    #plt.xlim(x0,max(energy_data[:,0]))
    plt.xlim(0,x1)
    #plt.plot([0,x1], [e_exact, e_exact], 'r--', label = 'Exact')
    plt.plot([0,x1], [1e-4, 1e-4], 'r--', label = 'Chemical Acc')
    plt.plot([0,x1], [abs(B-e_exact), abs(B-e_exact)], 'k--', label = 'Best Fit')
    plt.yscale("log")
    plt.legend()
    plt.show(block = False)
    try:
        input("Hit Enter to close")
    except:
        plt.close()
   
if __name__=='__main__':
    e_exact = -1.8746515745
    fname1 = 'Energyh4trial_r2.4N6rcut0.0005_dt0.05.out'
    fname2 = 'EQlanch4trial_r2.4N6rcut0.0005_dt0.05.out'
    e_exact = float(sys.argv[1])
    fname1 = sys.argv[2]
    fname2 = sys.argv[3]
    plot_energy(e_exact,fname1, fname2) 
