#!/usr/bin/env python
import model
import ansatz
import avaridyn
import pdb

nsite = 8
#ansatz = ansatz.ansatz(nsite, '0011100111', order = 10,rcut=1.e-2, op_filename='JW_uccsd_pool_nq10_np5.dat',opdense = True) #JW for H2O

#ansatz = ansatz.ansatz(nsite, '10110100', order = 10,rcut=1.e-3, op_filename='P_uccsd_pool_nq10_np6.dat',opdense = True) #Parity for H2O

ansatz = ansatz.ansatz(nsite, '00101101', order = 10,rcut=5.e-4, op_filename='P_uccsd_pool_nq10_np6.dat',opdense = True) #Parity for H2O



#filename = 'h2otrial_r1.5_jw.dat'
#filename = 'h2otrial_r0.8_jw.dat'
#filename = 'h2otrial_r0.8_parity.dat'
#filename = 'h2otrial_r1.0_parity.dat'
#filename = 'h2otrial_r1.5_parity.dat'
filename = 'h2otrial_r2.4_parity.dat'
#filename = "h2otrial_r2.2_parity.dat"
model = model.Molecule(nsite,filename )

dyn = avaridyn.avaridynMolecule(model, ansatz, dtmax=0.1, dthmax=0.1)
dyn.run()
