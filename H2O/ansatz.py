# author: Yongxin Yao (yxphysice@gmail.com)
import itertools, numpy, h5py, os
import scipy.optimize, scipy.linalg, scipy.sparse
import pdb
import re
import time
import multiprocessing
from joblib import Parallel, delayed
import copy
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from matplotlib import rc
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter


#from pygrisb.run.timing import timeit


def bin2dec(x):
    '''
    converts a string of binary number
    to a integer decimal number
    '''
    x = x.strip()
    d = 0
    n = len(x)
    for i in range(n-1,-1,-1):
        d += float(x[i])*(2**(n-1-i))
    return int(d)

def construct_string(sind, nq):
    #from avdynamics.model import get_sxyz_ops
    #sops_list = get_sxyz_ops(nq)
    #pdb.set_trace()
    gate = ['I',"X","Y","Z"]
    op_pool = []
    for term in sind:
        op = ''
        #print(term)
        # Qubit 0 is in the extreme right
        for t in range(len(term)-1,0,-2):
            if term[t]==0:
               continue
            #op += gate[term[t]]+str(term[t-1])
            #op += sops_list[ term[t]-1 ][ term[t-1] ]
            op += gate[ term[t] ]+str( term[t-1] )
            #op += gate[term[t]]+str(term[t-1])
        op_pool.append(op)

    return op_pool

def construct_operator(sind, nq):
    from avdynamics.model import get_sxyz_ops
    sops_list = get_sxyz_ops(nq)
    #pdb.set_trace()
    #gate = ['I',"X","Y","Z"]
    op_pool = []
    for term in sind:
        op = 1
        #print(term)
        # Qubit 0 is in the extreme right
        #for t in range(len(term)-1,0,-2):
        # Qubit 0 is in the extreme left
        for t in range(0, len(term),2):
            if term[t]==0:
               continue
            #op *= sops_list[ term[t]-1 ][ term[t-1] ]
            op *= sops_list[ term[t+1]-1 ][ term[t] ]
        op_pool.append(op)

    return op_pool


def construct_single_string(sind, nq):
    #from avdynamics.model import get_sxyz_ops
    #sops_list = get_sxyz_ops(nq)
    #pdb.set_trace()
    term = sind
    gate = ['I',"X","Y","Z"]
    op = ''
    #print(term)
    # Qubit 0 is in the extreme right
    #for t in range(len(term)-1,0,-2):
    for t in range(1,len(term),2):
        if term[t]==0:
           continue
        #op += gate[term[t]]+str(term[t-1])
        #op += sops_list[ term[t]-1 ][ term[t-1] ]
        op += gate[ term[t] ]+str( term[t-1] )
        #op += gate[term[t]]+str(term[t-1])

    return op

def construct_single_operator(sind, nq):
    from avdynamics.model import get_sxyz_ops
    sops_list = get_sxyz_ops(nq)
    op = 1
    #print(term)
    # Qubit 0 is in the extreme right
    #for t in range(len(term)-1,0,-2):
    # Qubit 0 is in the extreme left
    for t in range(1, len(sind) ,2):
        if sind[t]==0:
           continue
        #op *= sops_list[ term[t]-1 ][ term[t-1] ]
        op *= sops_list[ sind[t]-1 ][ sind[t-1] ]

    return op

class spectra_data:
    # data type for more efficient expm calculation.
    # store the dense matrix and spectral representation
    def __init__(self,
            qobj,  # qobj from qutip
            ):
        self._a = qobj.full()
        self._w, self._v = numpy.linalg.eigh(self._a)

    def expm(self, alpha):
        # calculate exp(alpha*a)
        return (self._v*numpy.exp(alpha*self._w)).dot(self._v.T.conj())

    def dot(self, b):
        return self._a.dot(b)

    def matrix_element(self, v, vp):
        return v.conj().dot(self._a.dot(vp))

    def show(self):
        print (numpy.asarray(self._a))

class spectra_data2:
    # data type for more efficient expm calculation.
    # store the dense matrix and spectral representation
    def __init__(self,
            qobj,  # qobj from qutip
            ):
        self._a = qobj.full()
        #self._w, self._v = numpy.linalg.eigh(self._a)

    def expm(self, alpha):
        # calculate exp(alpha*a)
        return scipy.linalg.expm(alpha*numpy.asarray(self._a))
        #return numpy.exp(alpha*numpy.asarray(self._a))
        #return (self._v*numpy.exp(alpha*self._w)).dot(self._v.T.conj())

    def dot(self, b):
        return self._a.dot(b)

    def matrix_element(self, v, vp):
        return v.conj().dot(self._a.dot(vp))

    def show(self):
        print (numpy.asarray(self._a))

class ansatz:
    def __init__(self,
            nq,
            ref_state=None,
            order=2,
            rcut=3.e-4,  # McLachlan distance cut-off
            pthcut=5,  # maximal allowed \frac{\par theta}{\par t}
            op_filename = None,
            pool=None,  # pool generation option
            opdense=True,  # operator in dense matrix format
            ):
        self._params = []
        self._op_filename = op_filename
        self._params_init = None
        self._reduced_pool = False
        # stores the A_mu
        self._ansatz = [[], []]
        self._pool = pool
        self.set_order(order)
        self._ngates = [0]*self._order
        self._opdense = opdense
        self._nq = nq
        # ref state is
        self.set_ref_state(ref_state) #
        self._state = None
        self._rcut = rcut
        self._pthcut = pthcut
        self._pthmaxt = 0  # max \frac{\par theta}{\par t}
        self.generate_op_pools()
        #self.h5init() #

    def set_order(self, order):
        if self._pool is None:
            self._order = order
        elif self._pool == "XYZ" or self._pool == "-XYZ":
            self._order = 2
        elif self._pool == "H2":
            self._order = 2
        else:
            raise ValueError(f"pool = {self._pool} not available.")

    def h5init(self):
        if os.path.isfile("ansatzi.h5"):
            with h5py.File("ansatzi.h5", "r") as f:
                self._ref_state = f["/ref_state"][()]
                if "/ansatz" in f:
                    self._ansatz[0] = f["/ansatz"][()]
                    self._ansatz[1] = f["/ansatz_code"][()]
                    self._params = f["/params"][()]

    def construct_psi0(self):
        self.update_state()
        #self._params_init = self._params[:]
        if self._params_init is None:
            self._params_init = self._params[:]
        else:
            for i in range(len(self._params) - len(self._params_init)):
                self._params_init.append(0.)

    def show(self):
        '''
        Print the operators used in the ansatz
        '''
        gate = ["I","X","Y","Z"]
        op_pool = []
        #pdb.set_trace()
        for term in self._ansatz[1]:
            op = ''
            for t in range(0,len(term),2):
                if  term[t+1] == 0:
                    continue
                op += gate[term[t+1]]+str(term[t])
            op_pool.append(op)
        print(op_pool)
        pass
    #@timeit
    def additive_optimize(self, h, e_exact):
        f = open('uccsd.out','w')
        e_data = []
        if self._opdense:
            self._h = spectra_data(h)
        else:
            self._h = h
        self.optimize_params()
        print(f"initial cost = {self._cost:.6f}")
        print(f"initial cost = {self._cost:.6f}",file = f)
        iloop = 0
        while True:
            added = self.add_op()
            if not added:
                break
            self.optimize_params()
            self.update_state()
            print(f"iter {iloop}: cost = {self._cost:.6f}")
            print(f"iter {iloop}: cost = {self._cost:.6f}",file = f)
            iloop += 1
            e_data.append([iloop,self._cost])
            if abs(self._cost-e_exact[0]) < 1.e-4:
                break
        #print(f"Number of operators  in the final ansatz: {len(self._ansatz[0]):2.f}")
        print("Number of operators  in the final ansatz:"+ str(len(self._ansatz[0])))
        print("Number of operators  in the final ansatz:"+ str(len(self._ansatz[0])),file = f)
        f.close()
        e_data = numpy.asarray(e_data)
        plt.plot(e_data[:,0],e_data[:,1],'.',label = 'adapt-VQE')
        plt.plot([0,iloop], [e_exact[0], e_exact[0]], 'r--', label = 'Exact 0')
        plt.xlabel('steps')
        plt.ylabel('E')
        plt.show(block=False)
        try:
            input('Hit enter to close')
        except:
            plt.close()
        if self._params_init is None:
            self._params_init = self._params[:]
        else:
            for i in range(len(self._params) - len(self._params_init)):
                self._params_init.append(0.)
    @property
    def state(self):
        return self._state

    @property
    def ngates(self):
        return self._ngates[:]

    def get_cost(self):
        self.update_state()
        res = self._h.matrix_element(self._state, self._state)
        return res.real

    #@timeit
    def update_state(self):
        self._state = self.get_state()

    def generate_op_pools(self):
        if self._op_filename:
            self._op_pools = generate_op_pool_custom(self._nq,
                    self._order,
                    self._op_filename,
                    self._pool,
                    )
            #self._op_pools = [self._op_pools]
        else:
            self._op_pools = generate_op_pools(self._nq,
                    self._order,
                    self._pool,
                    )
        if self._opdense:
            t0 = time.time()
            num_cores = 1
            #pdb.set_trace()
            #self._op_pools[0][0] = Parallel(n_jobs=num_cores)(delayed(spectra_data2(op)) for op in self._op_pools[0][0])
            for i, op_pool in enumerate(self._op_pools):
                #self._op_pools[i][0] = [spectra_data(op) for op in op_pool[0]]
                self._op_pools[i][0] = Parallel(n_jobs=num_cores)\
                                      (delayed(spectra_data)(op) for op in op_pool[0])
                #pdb.set_trace()
            t1 = time.time()
        else:
            t0 = time.time()
            num_cores = 1
            #pdb.set_trace()
            #self._op_pools[0][0] = Parallel(n_jobs=num_cores)(delayed(spectra_data2(op)) for op in self._op_pools[0][0])
            for i, op_pool in enumerate(self._op_pools):
                #self._op_pools[i][0] = [spectra_data2(op) for op in op_pool[0]]
                self._op_pools[i][0] = Parallel(n_jobs=num_cores)\
                                     (delayed(spectra_data2)(op) for op in op_pool[0])
            t1 = time.time()

        print(f"Operator pool constructed in {t1-t0}sec")

    def set_ref_state(self, ref_state):


        if  (isinstance(ref_state, numpy.ndarray)) == False :
            #print(ref_state)
            #print(isinstance(ref_state, numpy.ndarray) )
            #pdb.set_trace()
            self._ref_state = numpy.zeros((2**self._nq), dtype=numpy.complex)
            idx = bin2dec(ref_state)
            self._ref_state[idx] = 1.
            #self._ref_state[0] = 1.
        else:
            if not isinstance(ref_state, numpy.ndarray):
                ref_state = ref_state.full().reshape(-1)
                self._ref_state = ref_state
        #print(self._ref_state)
        #pdb.set_trace()
        #self._ref_state = ref_state

    #@timeit
    def optimize_params(self):
        if len(self._params) > 0:
            res = scipy.optimize.minimize(fun_cost,
                    self._params,
                    args=(self._ansatz[0],
                            self._ref_state,
                            self._h,
                            ),
                    method='CG',
                    jac=fun_jac,
                    )
            if res.success:
                self._params = res.x.tolist()
                self._cost = res.fun
            else:
                print(res.message)
        else:
            self._cost = self.get_cost()

    #@timeit
    def add_op(self, tol=1.e-4):
        scores = self.get_pool_scores()
        ids = numpy.argsort(abs(scores))
        iadd = ids[-1]
        print("top 3 scores: "+ \
                f"{' '.join(f'{scores[i]:.2e}' for i in ids[-3:])}")
        if len(self._ansatz[1]) > 0  \
                and numpy.allclose(self._op_pools[0][1][iadd],  \
                self._ansatz[1][-1]):
            # no further improvement
            print(f"abort: pauli ops {self._ansatz[1][-1]}" + \
                    f" vs {self._op_pools[0][1][iadd]}")
            return False
        elif abs(scores[iadd]) < tol:
            print(f"converge: gradient = {scores[iadd]:.2e} too small.")
            return False
        else:
            self._ansatz[0].append(self._op_pools[0][0][iadd])
            # label
            self._ansatz[1].append(self._op_pools[0][1][iadd])
            self.update_ngates()
            print(f"op {self._ansatz[1][-1]} appended.")
            self._params.append(0.)
            return True

    def update_ngates(self):
        iorder = numpy.count_nonzero(self._ansatz[1][-1][1::2])
        self._ngates[iorder-1] += 1

    #@timeit
    def add_ops_dyn(self,mmdfile):
        #if self._opdense:
        hvec = self._h.dot(self._state)
        #else:
        #    hvec = self._h*self._state
        icyc = 0
        hvar = self._e2 - self._e**2
        while True:
            np = len(self._params)
            mmat = numpy.zeros((np+1, np+1))
            mmat[:np, :np] = self._mmat
            vvec = numpy.zeros((np+1))
            vvec[:np] = self._vvec
            val_max = self._distp
            pth_max = max(self._pthmax, self._pthcut)
            ichoice = None
            mmat_ch = None
            # <partial_vec|vec_cur>
            #if self._opdense:
            vpv_list = [numpy.vdot(v, self._state)  \
                    for v in self._vecp_list]
            #else:
            #    vpv_list = [v.overlap(self._state) for v in self._vecp_list]
            #print(self._op_pools[-1][0])
            for i, op, label in zip(itertools.count(),
                    self._op_pools[-1][0],
                    self._op_pools[-1][1]):
                # same op is the end one, skip
                if len(self._ansatz[1]) > 0 and  \
                        numpy.allclose(label, self._ansatz[1][-1]):
                    continue
                # mmat addition
                #if self._opdense:
                vecp_add = -0.5j*op.dot(self._state)
                #else:
                #    vecp_add = -0.5j*op*self._state
                #if self._opdense:
                vpv_add = numpy.vdot(vecp_add, self._state)
                acol = [numpy.vdot(v, vecp_add) + vpv*vpv_add  \
                        for v, vpv in zip(self._vecp_list, vpv_list)]
                acol.append(numpy.vdot(vecp_add, vecp_add) +  \
                        vpv_add*vpv_add)
                # vvec addition
                zes = numpy.vdot(vecp_add, hvec)

                #else:
                #    vpv_add = vecp_add.overlap(self._state)
                #    acol = [v.overlap(vecp_add) + vpv*vpv_add  \
                #            for v, vpv in zip(self._vecp_list, vpv_list)]
                #    acol.append(vecp_add.overlap(vecp_add) +  \
                #            vpv_add*vpv_add)
                    # vvec addition
                #    zes = vecp_add.overlap(hvec)

                mmat[:, -1] = numpy.asarray(acol).real
                # upper symmetric part
                mmat[-1, :] = mmat[:, -1]

                # real time
                #vvec[-1] = zes.imag
                #vvec[-1] -= (vpv_add*self._e).imag

                # Imaginary time
                vvec[-1] = -zes.real
                vvec[-1] += (vpv_add*self._e).real
                ## m_inv = numpy.linalg.pinv(mmat)

                m_inv = get_minv(mmat)
                dist_p = (vvec.conj().dot(m_inv).dot(vvec)).real
                pthvec = m_inv.dot(vvec)
                pthmax = numpy.max(numpy.abs(pthvec))

                #print(zes)
                #op.show()
                #print(i,dist_p)
                '''
                if dist_p > val_max + 1e-8 or \
                        (abs(hvar - dist_p) < self._rcut and  \
                        pthmax < pth_max):
                '''
                    # dist drop, larger time step, better condition
                if dist_p > val_max:

                    ichoice = i
                    val_max = dist_p
                    pth_max = pthmax
                    mmat_ch = mmat.copy()
                    vvec_ch = vvec.copy()
                    minv_ch = m_inv.copy()
                    vecp_add_ch = vecp_add.copy()

            dist = hvar - val_max
            diff = val_max - self._distp
            print(f'{dist:.10e}',file = mmdfile)
            #print(dist)
            #if ichoice is None or  \
            #        (diff < 1.e-8 and pth_max - self._pthmax < 1.e-6):
            if ichoice is None:
                raise ValueError("dynamic ansatz cannot further improve.")

            self._distp = val_max
            self._pthmax = pth_max
            self._mmat = mmat_ch
            self._minv = minv_ch
            self._vvec = vvec_ch
            self._ansatz[0].append(self._op_pools[-1][0][ichoice])
            self._ansatz[1].append(self._op_pools[-1][1][ichoice])
            self.update_ngates()
            self._vecp_list.append(vecp_add_ch)
            self._params.append(0)
            self._params_init.append(0.)

            print(f"pth_max: {pth_max:.6f}")
            print(f"add op: {self._ansatz[1][-1]}")
            print(f"icyc = {icyc}, dist = {dist:.2e}, improving {diff:.2e}")
            #pdb.set_trace()

            if dist < self._rcut or abs(diff) < 1.e-5 or icyc >= 7:
                break
            icyc += 1

    def get_state(self):
        return get_ansatz_state(self._params,
                        self._ansatz[0],
                        self._ref_state,
                        )

    def get_pool_scores(self):
        '''-0.5j <[h,op]> = im(<h op>)
        '''
        scores = []
        #if self._opdense:
        h_vec = self._h.dot(self._state)
        #else:
        #    h_vec = self._h*self._state
        for op in self._op_pools[0][0]:
            #if self._opdense:
            ov = op.dot(self._state)
            zes = numpy.vdot(h_vec, ov)
            #else:
            #    ov = op*self._state
            #    zes = h_vec.overlap(ov)
            scores.append(zes.imag)
        return numpy.array(scores)

    #@timeit
    def one_step(self, h, dtmax, dthmax, dt, mmdfile):
        # hamiltoinian
        #from avdynamics.model import _evolution
        #dt = 0.5
        #if self._opdense:
        self._h = spectra_data(h)
        #else:
        #    self._h = spectra_data(h)
            #self._h = spectra_data2(h)
            #self._h = h
        #else:
        #    self._h = h
        self.set_par_states()
        amat = self.get_amat()
        cvec, mp, vp, e, e2 = self.get_cvec_phase()
        # McLachlan's principle, including global phase contribution
        #from avdynamics.model import spinModel.get_evolution
        #from avdynamics.model import HydrogenChain.get_evolution
        #from avdynamics.model import spinModel._evolution
        #pdb.set_trace()
        #evolution = get_evolution()
        evolution = 'I'
        m = amat.real + numpy.asarray(mp).real
        if evolution == "R":
            v = cvec.imag - numpy.asarray(vp).imag
        elif evolution == "I":
            v = -cvec.real + numpy.asarray(vp).real

            #v = cvec.imag - numpy.asarray(vp).imag
            #pdb.set_trace()
            #v = -cvec.real #+ numpy.asarray(vp).real
            #v = cvec.real - numpy.asarray(vp).real
        self._mmat = m
        self._vvec = v
        self._e, self._e2 = e, e2
        # m_inv = numpy.linalg.pinv(m)
        m_inv = get_minv(m)
        # McLachlan distance
        dist_h2 = e2 - e**2
        print("Var: "+str(dist_h2))
        dist_p = (v.conj().dot(m_inv).dot(v)).real
        dist = dist_h2 - dist_p
        self._distp = dist_p
        self._minv = m_inv
        p_params = m_inv.dot(v)
        if len(p_params) > 0:
            pthmax = numpy.max(numpy.abs(p_params))
        else:
            pthmax = 0
        self._pthmax = pthmax
        print(f"initial MMD= {dist:.10e}",file = mmdfile)
        print(f"initial mcLachlan distance: {dist:.2e} pthmax: {pthmax:.2e}")
        if dist > self._rcut or pthmax > self._pthcut:
            self.add_ops_dyn(mmdfile)
        p_params = self._minv.dot(self._vvec)
        if len(p_params) > 0:
            pthmax = numpy.max(numpy.abs(p_params))
            self._pthmaxt = max(pthmax, self._pthmaxt)
            print(f"max element in p_params: {pthmax:.2f}")
            #dt = 0.01
            # if pthmax > 0:
                # dt = min(dtmax, dthmax/pthmax)
                # dt = dthmax/pthmax

        self._params = [p + pp*dt for p, pp in zip(self._params, p_params)]
        self.update_state()
        norm = self._state.dot(self._state)
        #pdb.set_trace()
        return dt, e, dist_h2, 1/norm

    def condition_check(self, msg, tol=1.e-6):
        if minv_error(self._mmat, self._minv) > tol:
            raise ValueError(f"{msg} M matrix inversion error.")

    def get_dist(self):
        return self._e2 - self._e**2 - self._distp

    #@timeit
    def set_par_states(self):
        ''' d |vec> / d theta.
        '''
        np = len(self._params)
        vecp_list = []
        vec_i = self._ref_state
        #if self._opdense:
        for i in range(np):
            th, op = self._params[i], self._ansatz[0][i]
            # factor of 0.5 difference from the main text.
            vec_i = op.expm(-0.5j*th).dot(vec_i)
            vec = -0.5j*op.dot(vec_i)
            for th, op in zip(self._params[i+1:], self._ansatz[0][i+1:]):
                vec = op.expm(-0.5j*th).dot(vec)
            vecp_list.append(vec)
        #else:
        #    for i in range(np):
        #        th, op = self._params[i], self._ansatz[0][i]
        #        opth = -0.5j*th*op
        #        vec_i = opth.expm()*vec_i
        #        vec = -0.5j*op*vec_i
        #        for th, op in zip(self._params[i+1:], self._ansatz[0][i+1:]):
        #            opth = -0.5j*th*op
        #            vec = opth.expm()*vec
        #        vecp_list.append(vec)
        self._vecp_list = vecp_list

    def get_amat(self):
        np = len(self._params)
        amat = numpy.zeros((np, np), dtype=numpy.complex)
        for i in range(np):
            for j in range(i, np):
                #if self._opdense:
                zes = numpy.vdot(self._vecp_list[i], self._vecp_list[j])
                #else:
                #    zes = self._vecp_list[i].overlap(self._vecp_list[j])
                amat[i, j] = zes
                if i != j:
                    # hermitian component
                    amat[j, i] = numpy.conj(zes)
        return amat

    #@timeit
    def get_cvec_phase(self):
        np = len(self._params)
        cvec = numpy.zeros(np, dtype=numpy.complex)
        # h |vec>
        #if self._opdense:
        hvec = self._h.dot(self._state)
        for i in range(np):
            cvec[i] = numpy.vdot(self._vecp_list[i], hvec)
        # energy
        e = numpy.vdot(self._state, hvec).real
        e2 = numpy.vdot(hvec, hvec).real
        # <partial_vec|vec_cur>
        vpv_list = [numpy.vdot(vecp, self._state)  \
                for vecp in self._vecp_list]
        #else:
        #    hvec = self._h*self._state
        #    for i in range(np):
        #        cvec[i] = self._vecp_list[i].overlap(hvec)
        #    # energy
        #    e = (self._state.overlap(hvec)).real
        #    e2 = (hvec.overlap(hvec)).real
            # <partial_vec|vec_cur>
        #    vpv_list = [vecp.overlap(self._state)  \
        #            for vecp in self._vecp_list]
        mp = [[vi*vj for vj in vpv_list] for vi in vpv_list]
        vp = [vi*e for vi in vpv_list]
        return cvec, mp, vp, e, e2

    def save_ansatz(self):
        with h5py.File("ansatz.h5", "w") as f:
            # initial state params
            f["/params"] = self._params_init
            # ansatz operator labels
            f["/ansatz_code"] = self._ansatz[1]
            # ngates
            f["/ngates"] = self._ngates
            # reference state
            f["/ref_state"] = self._ref_state


def minv_error(m, minv):
    n = m.shape[0]
    res = minv.dot(m)
    res = numpy.max(numpy.abs(res - numpy.eye(n)))
    return res


def get_minv(a, delta=1.e-6):
    ap = a + delta*numpy.eye(a.shape[0])
    ainv = numpy.linalg.pinv(ap)
    return ainv


def fun_cost(params, ansatz, ref_state, h):
    state = get_ansatz_state(params, ansatz, ref_state)
    res = h.matrix_element(state, state)
    return res.real


def fun_jac(params, ansatz, ref_state, h):
    # - d <var|h|var> / d theta
    np = len(ansatz)
    vec = get_ansatz_state(params, ansatz, ref_state)
    opdense = isinstance(vec, numpy.ndarray)
    # <vec|h
    if opdense:
        h_vec = h.dot(vec)
    else:
        h_vec = h*vec

    jac = []
    state_i = ref_state
    if opdense:
        for i in range(np):
            op = ansatz[i]
            state_i = op.expm(-0.5j*params[i]).dot(state_i)
            state = op.dot(state_i)
            for theta, op in zip(params[i+1:], ansatz[i+1:]):
                state = op.expm(-0.5j*theta).dot(state)
            zes = numpy.vdot(h_vec, state)
            jac.append(zes.imag)
    else:
        for i in range(np):
            opth = -0.5j*params[i]*ansatz[i]
            state_i = opth.expm()*state_i
            state = op*state_i
            for theta, op in zip(params[i+1:], ansatz[i+1:]):
                opth = -0.5j*theta*op
                state = opth.expm()*state
            zes = h_vec.overlap(state)
            jac.append(zes.imag)
    res = numpy.array(jac)
    return res


def get_ansatz_state(params, ansatz, ref_state):
    state = ref_state
    opdense = isinstance(state, numpy.ndarray)
    #pdb.set_trace()
    for theta, op in zip(params, ansatz):
        #if opdense:
        opth_expm = op.expm(-0.5j*theta)
        state = opth_expm.dot(state)
        #else:
        #    opth = -0.5j*theta*op
        #    state = opth.expm()*state
    #pdb.set_trace()
    return state

def rotate_z(pstring):
    for i, t in enumerate(pstring):
        if i%2==1:
            if t == 2: # make Y to X
                pstring[i] = 1
            elif t == 1: # make X to Y
                pstring[i] = 2


#@timeit
def generate_op_pools(nq,
        order,
        pool,
        ):
    from avdynamics.model import get_sxyz_ops
    sops_list = get_sxyz_ops(nq)

    # operator and label
    op_pools = []
    op_pool = [[], []]
    sind_temp = numpy.zeros(order*2, dtype=numpy.int)
    if pool is None or pool[0] == '-':
        for iorder in range(0,order):
        #for iorder in range(order-1,order):
            for ii in itertools.combinations(range(nq), iorder+1):
                for ss in itertools.product(range(3), repeat=iorder+1):
                    if ss.count(1)%2 == 0:
                        continue
                    op = 1
                    sind = sind_temp.copy()
                    for n, i, s in zip(itertools.count(), ii, ss):
                        op *= sops_list[s][i]
                        sind[2*n:2*n+2] = [i, s+1]
                    op_pool[0].append(op)
                    op_pool[1].append(sind)
        op_pools.append(op_pool)

    # Apply Z-axis rotation symmetry
    #num_cores = multiprocessing.cpu_count()
    '''
    t0 = time.time()
    for term in op_pools[0][1]:
        #print(term)
        x = copy.deepcopy(term)
        rotate_z(x)
        #idx = Parallel(n_jobs=num_cores)\
        #     (delayed(int)(i) for i,s in enumerate(op_pools[0][1]) if str(x) == str(s) )
        idx = [i for i,s in enumerate(op_pools[0][1]) if str(x) == str(s)]
        #if len(idx)>0:
        #    print(idx)
            #pdb.set_trace()
        for i in idx[0:]:
            del op_pools[0][0][i]
            del op_pools[0][1][i]

    t1 = time.time()
    print(f'Z-rotation symmetry applied in {t1-t0}sec.')
    #'''



    print(f"op pool size: {len(op_pool[0])} for op_pool in op_pools")
    #pdb.set_trace()
    return op_pools


def generate_op_pools_jw(nq,
        order,
        pool,
        ):
    '''
    recursively generates operator pool
    '''
    gate = ['I',"X","Y","Z"]

    if nq == 2:
        #op_pool = []
        #sind = [ [0,2,1,3,2,3],[0,0,1,2,2,3],[0,0,1,0,2,2],[0,0,1,2,2,0] ]
        sind = [ [0,2,1,3],[0,0,1,2] ]
        #op_pool = construct_string(sind)
        op_pool = construct_operator(sind,nq)
        op_str = construct_string(sind,nq)
        return [op_pool, sind, op_str]
    else:
        tmp = generate_op_pools_jw(nq-1, nq-1, None)
        sind = []
        #ss = []
        for term in tmp[1]:
            term.append(nq-1)
            term.append(3)
            sind.append(term)
        term1 = [0]*(2*nq)
        c = 0
        for t in range(len(term)):
            if t%2 == 0:
                term1[t] = c
                c += 1
        term1[2*nq-1] = 2

        term2 = [0]*(2*nq)
        c = 0
        for t in range(len(term)):
            if t%2 == 0:
                term2[t] = c
                c += 1
        term2[2*nq-3] = 2
        sind.append(term1)
        sind.append(term2)
        pool = construct_operator(sind,nq)
        pool_str = construct_string(sind,nq)
        #pdb.set_trace()
        return  [pool, sind, pool_str]


def generate_op_pools_parity(nq,
        order,
        pool,
        ):
    '''
    recursively generates operator pool for parity encoding
    '''
    gate = ['I',"X","Y","Z"]

    if nq == 2:
        #op_pool = []
        sind = [[0,2,1,1], [0,2,1,0]]
        #op_pool = construct_string(sind)
        op_pool = construct_operator(sind,nq)
        return [op_pool, sind]
    else:
        tmp = generate_op_pools_parity(nq-1, nq-1, None)
        sind = []
        #ss = []
        for term in tmp[1]:
            term.append(nq-1)
            term.append(3)
            sind.append(term)
        term1 = [0]*(2*nq)
        c = 0
        for t in range(len(term)):
            if t%2 == 0:
                term1[t] = c
                c += 1
        term1[2*nq-1] = 2

        term2 = [0]*(2*nq)
        c = 0
        for t in range(len(term)):
            if t%2 == 0:
                term2[t] = c
                c += 1
        term2[2*nq-3] = 2
        sind.append(term1)
        sind.append(term2)
        pool = construct_operator(sind,nq)
        #pdb.set_trace()
        return  [pool, sind]

def generate_op_pool_custom(nq,
        order,
        op_file_name,
        pool,
       ):
    '''
    read operators from a custom file
    '''
    from avdynamics.model import get_sxyz_ops
    sops_list = get_sxyz_ops(nq)

    paulis = {'I':0,'X':1,'Y':2,'Z':3}
    # operator and label
    op_pools = []
    op_pool = [[], []]
    #'''
    f0 = open(op_file_name,"r")
    while True:
        line = f0.readline().strip()
        if len(line) < 1:
            break
        #print(line)
        sind = []
        for site, pauli in enumerate(list(line)):
            sind.append(nq-1-site)
            sind.append(paulis[pauli])
        # remove terms with even number of Y terms
        if sind[1::2].count(2)%2 == 0:
            print(line)
            #pdb.set_trace()
            continue
        #print(sind)
        op = construct_single_operator(sind, nq)
        #p_string = construct_single_string(sind, nq)
        #print(p_string)
        op_pool[0].append(op)
        op_pool[1].append(sind)
        #pdb.set_trace()
    op_pools.append(op_pool)


    # Apply Z-axis rotation symmetry
    #'''
    for term in op_pools[0][1]:
        #print(term)
        #print(len(op_pools[0][1]))
        x = copy.deepcopy(term)
        rotate_z(x)
        #pdb.set_trace()
        #idx = Parallel(n_jobs=num_cores)\
        #     (delayed(int)(i) for i,s in enumerate(op_pools[0][1]) if str(x) == str(s) )
        idx = [i for i,s in enumerate(op_pools[0][1]) if str(x) == str(s)]
        #if len(idx)>0:
        #    print(idx)
            #pdb.set_trace()
        for i in idx[0:]:
            del op_pools[0][0][i]
            del op_pools[0][1][i]

    #print(len(op_pools[0][0]))
    f0.close()
    #'''


    print(f"op pool size: {len(op_pool[0])} for op_pool in op_pools")
    #pdb.set_trace()
    return op_pools


if __name__=='__main__':

    n = bin2dec('001001')
    print(n)
    #ans1 = generate_op_pools_jw(4,4,None)
    #ans1 = generate_op_pools_parity(2,2,None)
    ans2 = generate_op_pool_custom(8,6,'uccsd_pool_nq8.dat',None)
    #print(ans1)
    #pdb.set_trace()
    #print(ans2)
