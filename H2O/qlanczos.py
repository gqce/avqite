import numpy as np
from scipy import linalg as SciLA
import matplotlib.pyplot as plt


def qlanc(i,data,ds,dlanz=1e-2):
    E = data[:,1]
    norm = data[:,2]
    vect = list(np.arange(1,i+2,2))
    n = len(vect)

    H = np.zeros([n,n],dtype=complex)
    S = np.zeros([n,n],dtype=complex)
    j = 0
    k = 0
    for l in vect:
        k = 0
        for lp in vect:
            S[j,k] = norm[(l+lp)//2]**2/(norm[l]*norm[lp])
            H[j,k] = E[(l+lp)//2]*S[j,k]
            k += 1
        j += 1
    # Determine which vectors to keep to stabilize lanczos scheme
    idx = [0]
    ii = 0
    jj = 0
    while(ii<n and jj<n-1):
        for jj in range(ii+1,n):
            if(S[ii,jj]<ds):
                idx.append(jj)
                break
        ii = idx[len(idx)-1]
    if S.shape[0]-1 not in idx:
        idx += [S.shape[0]-1]
    H_ = H[idx,:]
    H_ = H_[:,idx]
    S_ = S[idx,:]
    S_ = S_[:,idx]
    n = S_.shape[0]
    # Regularize the overlap matrix
    sigma,V = np.linalg.eigh(S_)
    jdx = np.where(sigma>dlanz)[0]
    S_ = np.dot(V.T,np.dot(S_,V))
    H_ = np.dot(V.T,np.dot(H_,V))
    S_ = S_[jdx,:]
    S_ = S_[:,jdx]
    H_ = H_[jdx,:]
    H_ = H_[:,jdx]
  
    eps,c = SciLA.eig(H_,S_)
    #eps,c = SciLA.eig(H,S)
    return np.min(eps)


def qlanc2(i,data,ds,dlanz=1e-2):
    E = data[:,1]
    norm = data[:,2]
    vect = list(np.arange(1,i+2,2))
    n = len(vect)

    H = np.zeros([n,n],dtype=complex)
    S = np.zeros([n,n],dtype=complex)
    j = 0
    k = 0
    for l in vect:
        k = 0
        for lp in vect:
            S[j,k] = norm[(l+lp)//2]**2/(norm[l]*norm[lp])
            H[j,k] = E[(l+lp)//2]*S[j,k]
            k += 1
        j += 1
    # Determine which vectors to keep to stabilize lanczos scheme
    idx = [0]
    ii = 0
    jj = 0
    while(ii<n and jj<n-1):
        for jj in range(ii+1,n):
            if(S[ii,jj]<ds):
                idx.append(jj)
                break
        ii = idx[len(idx)-1]
    if S.shape[0]-1 not in idx:
        idx += [S.shape[0]-1]
    H_ = H
    S_ = S
    n = S.shape[0]
    # Regularize the overlap matrix
    sigma,V = np.linalg.eigh(S_)
    jdx = np.where(sigma>dlanz)[0]
    S_ = np.dot(V.T,np.dot(S_,V))
    H_ = np.dot(V.T,np.dot(H_,V))
    S_ = S_[jdx,:]
    S_ = S_[:,jdx]
    H_ = H_[jdx,:]
    H_ = H_[:,jdx]
  
    eps,c = SciLA.eig(H_,S_)
    return np.min(eps)

if __name__ == "__main__":
   ds = 0.9
   data = np.loadtxt("qlanczos_h2.out",dtype = complex) 
   nstep = len(data[:,0])
   eq = []
   for i in range(1,nstep,2):
       e = qlanc(i,data,ds,dlanz=1e-14)
       eq.append([data[i,0],e])
       print(e)
   eq = np.asarray(eq)
   plt.plot(data[:,0],data[:,1], label = "QITE")
   plt.plot(eq[:,0],eq[:,1], label = "Qlanc")
   plt.legend()
   plt.show(block = False)
   try:
       input("Hit Enter to close")
   except:
       plt.close()

